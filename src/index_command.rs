use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(name = "index", about = "Program for updating the index")]
pub enum IndexCommand {
    Update {
        #[structopt(short = "f", long = "force")]
        force: bool 
    },
    Start { 
        #[structopt(short = "p", long = "pause-time", default_value = "3600")]
        pause_time: u32 
    },
    Stop,
    Add {
        #[structopt(short = "p", long = "path")]
        path: String 
    },
    Remove { 
        #[structopt(short = "p", long = "path")]
        path: String 
    },
}

impl IndexCommand {
    pub fn get() -> IndexCommand {
        IndexCommand::from_args()
    }

    pub fn execute(self) {
        match self {
            IndexCommand::Update { force } => self.update(force),
            IndexCommand::Start { pause_time } => self.start(pause_time),
            IndexCommand::Stop => self.stop(),
            _ => (), 
        }
    }

    pub fn update(self, force: bool) {

    }

    pub fn start(self, pause_time: u32) {

    }

    pub fn stop(self) {

    }
}